import { Controller, Get, Query, Req } from '@nestjs/common';
import { QuizService } from '../../quiz/quiz.service';
import { UserRequest } from '../../auth/dto/auth.dto';
import { QueryStudentAnswersDto } from './dto/query-student-answers.dto';

@Controller('teacher/student-answers')
export class StudentAnswersTeacher {
    constructor(private quizService: QuizService) {}

    @Get('/')
    async getStudentAnswers(@Req() { user }: UserRequest, @Query() query: QueryStudentAnswersDto) {
        return this.quizService.getStudentAnswers(query);
    }
}
