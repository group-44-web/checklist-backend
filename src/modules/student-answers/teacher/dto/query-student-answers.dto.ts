import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';
import { QueryDto } from '../../../../utils/common-dto.util';

export class QueryStudentAnswersDto extends QueryDto {
    @Type(() => Number)
    @IsNumber()
    classId: number;

    @Type(() => Number)
    @IsNumber()
    studentId: number;
}
